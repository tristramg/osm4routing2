package reader

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadInternals(t *testing.T) {
	nodes, _ := readNodes("test_data/minimal.osm.pbf")
	assert.Equal(t, 2, len(nodes), "Wrong number of nodes")

	ways, _ := readWays("test_data/minimal.osm.pbf")
	assert.Equal(t, 1, len(ways), "Wrong number of ways")
	assert.Equal(t, 2, len(ways[0].nodes), "Wrong number of nodes ids in way")
}

func TestReadAll(t *testing.T) {
	nodes, ways, _ := Read("test_data/minimal.osm.pbf")
	assert.Equal(t, 2, len(nodes), "Wrong number of nodes")
	assert.Equal(t, 1, len(ways), "Wrong number of ways")
}

func TestSelectEdgeNodes(t *testing.T) {
	ways := []Way{{0, []int64{1, 2, 3}}}
	nodes := map[int64]Node{
		1: {1, Coord{0, 0}, 0},
		2: {2, Coord{0, 0}, 0},
		3: {3, Coord{0, 0}, 0},
	}

	countNodesUses(nodes, ways)
	filtered := selectNodes(nodes, ways)
	assert.Equal(t, len(filtered), 2, "Wrong number of filtered nodes")
	if filtered[1].ID != 1 || filtered[3].ID != 3 {
		t.Errorf("Wrong selected nodes, %v", filtered)
	}
}

func TestSelectIntersectionNodes(t *testing.T) {
	ways := []Way{
		{0, []int64{1, 2, 3}},
		{1, []int64{4, 2, 5}},
	}
	nodes := map[int64]Node{
		1: {1, Coord{0, 0}, 0},
		2: {2, Coord{0, 0}, 0},
		3: {3, Coord{0, 0}, 0},
		4: {4, Coord{0, 0}, 0},
		5: {5, Coord{0, 0}, 0},
	}

	countNodesUses(nodes, ways)
	filtered := selectNodes(nodes, ways)
	assert.Equal(t, 5, len(filtered), "Wrong number of filtered nodes")
}

func TestImpossibleRead(t *testing.T) {
	_, err := readNodes("kittens.avi")
	assert.Error(t, err)

	_, err = readWays("nevergonnagiveyouup.flv")
	assert.Error(t, err)

	_, _, err = Read("yetAnotherJoke.mkv")
	assert.Error(t, err)
}

func TestDistance(t *testing.T) {
	d := distance(Coord{0, 0}, Coord{0, 1 / 60.0})
	assert.InEpsilon(t, 1852, d, 1)
}

func TestSplitWayNoIntersection(t *testing.T) {
	way := Way{0, []int64{1, 2, 3}}
	nodes := map[int64]Node{
		1: {1, Coord{0, 0}, 1},
		2: {2, Coord{1, 0}, 1},
		3: {3, Coord{2, 0}, 1},
	}
	split := splitWay(way, nodes)
	assert.Equal(t, len(split), 1)
	assert.Equal(t, split[0].Source, int64(1))
	assert.Equal(t, split[0].Target, int64(3))
	assert.InEpsilon(t, split[0].Length(), 2*60*1852, 1)
}

func TestSplitWayWithIntersection(t *testing.T) {
	way := Way{0, []int64{1, 2, 3}}
	nodes := map[int64]Node{
		1: {1, Coord{0, 0}, 1},
		2: {2, Coord{1, 0}, 2},
		3: {3, Coord{2, 0}, 1},
	}
	split := splitWay(way, nodes)
	assert.Equal(t, len(split), 2)
	assert.Equal(t, split[0].Source, int64(1))
	assert.Equal(t, split[0].Target, int64(2))
	assert.InEpsilon(t, split[0].Length(), 1*60*1852, 1)

	assert.Equal(t, split[1].Source, int64(2))
	assert.Equal(t, split[1].Target, int64(3))
	assert.InEpsilon(t, split[1].Length(), 1*60*1852, 1)
}

func TestEdgeAsWKT(t *testing.T) {
	var edge Edge
	edge.Geometry = []Coord{
		Coord{0, 0},
		Coord{1, 0},
		Coord{0, 1},
	}
	assert.Equal(t, "LINESTRING(0.0000000 0.0000000, 1.0000000 0.0000000, 0.0000000 1.0000000)", edge.AsWKT())
}
