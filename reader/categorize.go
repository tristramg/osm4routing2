package reader

const (
	// Unknown accessiblity
	Unknown = -1

	// FootForbidden that no pedestrian is allowed
	FootForbidden = 0
	// FootAllowed pedestrians are allowed in both directions
	FootAllowed = 1

	// CarForbidden no car is allowed
	CarForbidden = 0
	// CarResidential http://wiki.openstreetmap.org/wiki/Tag:highway%3Dresidential
	CarResidential = 1
	// CarTertiary http://wiki.openstreetmap.org/wiki/Tag:highway%3Dtertiary
	CarTertiary = 2
	// CarSecondary http://wiki.openstreetmap.org/wiki/Tag:highway%3Dsecondary
	CarSecondary = 3
	// CarPrimary http://wiki.http://wiki.openstreetmap.org/wiki/Tag:highway%3Dprimary
	CarPrimary = 4
	// CarTrunk http://wiki.openstreetmap.org/wiki/Tag:highway%3Dtrunk
	CarTrunk = 5
	// CarMotorway http://wiki.openstreetmap.org/wiki/Tag:highway%3Dmotorway
	CarMotorway = 6

	// BikeForbidden bike can not use this edge
	BikeForbidden = 0
	// BikeOppositeLane bike can use this edge, in opposite direction of main traffic
	BikeOppositeLane = 1
	// BikeAllowed means that it can be used by a bike, but the traffic might be shared with cars or pedestrians
	BikeAllowed = 2
	// BikeLane is a narrow lane dedicated for bike, without physical separation from other traffic
	BikeLane = 3
	// BikeBusway means that bikes are allowed on the bus lane
	BikeBusway = 4
	// BikeTrack is a physically separated for any other traffic
	BikeTrack = 5
)

// EdgeProperties contains what mode can use the edge in each direction
type EdgeProperties struct {
	Foot                      int
	CarForward, CarBackward   int
	BikeForward, BikeBackward int
}

// NewEdgeProperties builds an EdgeProperties where every property is unknown
func NewEdgeProperties() *EdgeProperties {
	return &EdgeProperties{
		Foot:         Unknown,
		CarForward:   Unknown,
		CarBackward:  Unknown,
		BikeForward:  Unknown,
		BikeBackward: Unknown,
	}
}

// Normalize fills unknown fields
func (properties *EdgeProperties) Normalize() {
	if properties.CarBackward == Unknown {
		properties.CarBackward = properties.CarForward
	}
	if properties.BikeBackward == Unknown {
		properties.BikeBackward = properties.BikeForward
	}
	if properties.CarForward == Unknown {
		properties.CarForward = CarForbidden
	}
	if properties.BikeForward == Unknown {
		properties.BikeForward = BikeForbidden
	}
	if properties.CarBackward == Unknown {
		properties.CarBackward = CarForbidden
	}
	if properties.BikeBackward == Unknown {
		properties.BikeBackward = BikeForbidden
	}
	if properties.Foot == Unknown {
		properties.Foot = FootForbidden
	}
}

// Accessible means that at least one mean of transportation can use it in one direction
func (properties *EdgeProperties) Accessible() bool {
	return properties.BikeForward != BikeForbidden || properties.BikeBackward != BikeForbidden ||
		properties.CarForward != CarForbidden || properties.CarBackward != CarForbidden ||
		properties.Foot != FootForbidden
}

func (properties *EdgeProperties) update(key string, val string) {
	switch key {
	case "highway":
		switch val {
		case "cycleway", "path", "footway", "steps", "pedestrian":
			properties.BikeForward = BikeTrack
			properties.Foot = FootAllowed
		case "primary", "primary_link":
			properties.CarForward = CarPrimary
			properties.Foot = FootAllowed
			properties.BikeForward = BikeAllowed
		case "secondary":
			properties.CarForward = CarSecondary
			properties.Foot = FootAllowed
			properties.BikeForward = BikeAllowed
		case "tertiary":
			properties.CarForward = CarTertiary
			properties.Foot = FootAllowed
			properties.BikeForward = BikeAllowed
		case "unclassified", "residential", "living_street", "road", "service", "track":
			properties.CarForward = CarResidential
			properties.Foot = FootAllowed
			properties.BikeForward = BikeAllowed
		case "motorway", "motorway_link":
			properties.CarForward = CarMotorway
			properties.Foot = CarForbidden
			properties.BikeForward = BikeForbidden
		case "trunk", "trunk_link":
			properties.CarForward = CarTrunk
			properties.Foot = CarForbidden
			properties.BikeForward = BikeForbidden
		}
	case "pedestrian", "foot":
		switch val {
		case "no":
			properties.Foot = FootForbidden
		default:
			properties.Foot = FootAllowed
		}

	// http://wiki.openstreetmap.org/wiki/Cycleway
	// http://wiki.openstreetmap.org/wiki/Map_Features#Cycleway
	case "cycleway":
		switch val {
		case "track":
			properties.BikeForward = BikeTrack
		case "opposite_track":
			properties.BikeBackward = BikeTrack
		case "opposite":
			properties.BikeBackward = BikeAllowed
		case "share_busway":
			properties.BikeForward = BikeBusway
		case "lane_left", "opposite_lane":
			properties.BikeBackward = BikeLane
		default:
			properties.BikeForward = BikeLane
		}

	case "bicycle":
		switch val {
		case "no", "false":
			properties.BikeForward = BikeForbidden
		default:
			properties.BikeForward = BikeAllowed
		}
	case "busway":
		switch val {
		case "opposite_lane", "opposite_track":
			properties.BikeBackward = BikeBusway
		default:
			properties.BikeForward = BikeBusway
		}

	case "oneway":
		switch val {
		case "yes", "true", "1":
			properties.CarBackward = CarForbidden
			if properties.BikeBackward == Unknown {
				properties.BikeBackward = BikeForbidden
			}
		}

	case "junction":
		switch val {
		case "roundabout":
			properties.CarBackward = CarForbidden
			if properties.BikeBackward == Unknown {
				properties.BikeBackward = BikeForbidden
			}
		}
	}
}
