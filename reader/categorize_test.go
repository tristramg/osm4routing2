package reader

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAccessible(t *testing.T) {
	p := NewEdgeProperties()
	p.Normalize()
	assert.False(t, p.Accessible(), "By default an edge should not be accessible")

	p.Foot = FootAllowed
	assert.True(t, p.Accessible(), "The edge should be accessible")
}

func TestNormalize(t *testing.T) {
	p := NewEdgeProperties()
	p.BikeForward = BikeLane
	p.Normalize()
	assert.Equal(t, BikeLane, p.BikeBackward, "Normalization failed")
	assert.True(t, p.Accessible(), "The edge should be accessible")
	p.BikeForward = BikeAllowed
	p.Normalize()
	assert.Equal(t, BikeLane, p.BikeBackward, "Normalization should not override values")

	p.CarForward = CarSecondary
	p.CarBackward = Unknown
	p.Normalize()
	assert.Equal(t, CarSecondary, p.CarBackward, "Normalization failed")
}

func TestUpdate(t *testing.T) {
	p := NewEdgeProperties()
	p.update("highway", "secondary")
	assert.Equal(t, CarSecondary, p.CarForward)

	p.update("highway", "primary_link")
	assert.Equal(t, CarPrimary, p.CarForward)

	p.update("highway", "motorway")
	assert.Equal(t, CarMotorway, p.CarForward)

	p.update("highway", "residential")
	assert.Equal(t, CarResidential, p.CarForward)

	p.update("highway", "tertiary")
	assert.Equal(t, CarTertiary, p.CarForward)

	p.update("highway", "trunk")
	assert.Equal(t, CarTrunk, p.CarForward)

	p.update("highway", "cycleway")
	assert.Equal(t, BikeTrack, p.BikeForward)
	assert.Equal(t, FootAllowed, p.Foot)

	p.update("foot", "designated")
	assert.Equal(t, FootAllowed, p.Foot)

	p.update("foot", "no")
	assert.Equal(t, FootForbidden, p.Foot)

	p.update("cycleway", "lane")
	assert.Equal(t, BikeLane, p.BikeForward)

	p.update("cycleway", "track")
	assert.Equal(t, BikeTrack, p.BikeForward)

	p.update("cycleway", "opposite_lane")
	assert.Equal(t, BikeLane, p.BikeBackward)

	p.update("cycleway", "opposite_track")
	assert.Equal(t, BikeTrack, p.BikeBackward)

	p.update("cycleway", "opposite")
	assert.Equal(t, BikeAllowed, p.BikeBackward)

	p.update("cycleway", "share_busway")
	assert.Equal(t, BikeBusway, p.BikeForward)

	p.update("cycleway", "lane_left")
	assert.Equal(t, BikeLane, p.BikeBackward)

	p.update("bicycle", "yes")
	assert.Equal(t, BikeAllowed, p.BikeForward)

	p.update("bicycle", "no")
	assert.Equal(t, BikeForbidden, p.BikeForward)

	p.update("busway", "yes")
	assert.Equal(t, BikeBusway, p.BikeForward)

	p.update("busway", "opposite_track")
	assert.Equal(t, BikeBusway, p.BikeBackward)

	p.update("oneway", "yes")
	assert.Equal(t, BikeForbidden, p.CarBackward)
	assert.NotEqual(t, p.BikeBackward, BikeForbidden)

	p.BikeBackward = Unknown
	p.update("oneway", "yes")
	assert.Equal(t, BikeForbidden, p.BikeBackward)

	p.update("junction", "roundabout")
	assert.Equal(t, BikeForbidden, p.CarBackward)

	p.BikeBackward = Unknown
	p.update("junction", "roundabout")
	assert.Equal(t, BikeForbidden, p.BikeBackward)
}
