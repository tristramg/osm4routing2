package reader

import (
	"fmt"
	"io"
	"math"
	"os"
	"runtime"

	"github.com/qedus/osmpbf"
)

// Coord are coordinates in decimal degress WGS84
type Coord struct {
	Lon, Lat float64
}

// Node is the OpenStreetMap node
type Node struct {
	ID    int64
	Coord Coord
	uses  int64
}

// Way as represented in OpenStreetMap
type Way struct {
	ID    int64
	nodes []int64
}

// Edge is a topological representation with only two extremities and no geometry
type Edge struct {
	Source, Target int64
	// Length in meters
	Geometry []Coord
}

// Length in meters of the edge
func (edge Edge) Length() (length float64) {
	for i, coord := range edge.Geometry {
		if i != 0 {
			length += distance(edge.Geometry[i-1], coord)
		}
	}
	return
}

// AsWKT returns the geometry in the Well Known Format
func (edge Edge) AsWKT() (wkt string) {
	wkt = "LINESTRING("
	for i, coord := range edge.Geometry {
		if i == len(edge.Geometry)-1 {
			// 7 digits is about 1cm precision
			wkt += fmt.Sprintf("%.7f %.7f)", coord.Lon, coord.Lat)
		} else {
			wkt += fmt.Sprintf("%.7f %.7f, ", coord.Lon, coord.Lat)
		}
	}
	return
}

func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}

func distance(a, b Coord) float64 {
	// convert to radians
	// must cast radius as float to multiply later
	var la1, lo1, la2, lo2, r float64
	la1 = a.Lat * math.Pi / 180
	lo1 = a.Lon * math.Pi / 180
	la2 = b.Lat * math.Pi / 180
	lo2 = b.Lon * math.Pi / 180

	r = 6378100 // Earth radius in METERS

	// calculate
	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)

	return 2 * r * math.Asin(math.Sqrt(h))
}

func splitWay(way Way, nodes map[int64]Node) []Edge {
	var result []Edge
	var edge Edge
	for i, nodeID := range way.nodes {
		node := nodes[nodeID]
		if i == 0 {
			edge.Source = nodeID
			edge.Geometry = []Coord{node.Coord}
		} else {
			edge.Geometry = append(edge.Geometry, node.Coord)

			if node.uses > 1 || i == len(way.nodes)-1 {
				edge.Target = nodeID
				result = append(result, edge)

				edge.Source = nodeID
				edge.Geometry = []Coord{node.Coord}
			}
		}
	}
	return result
}

func readNodes(filename string) (map[int64]Node, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	d := osmpbf.NewDecoder(f)
	err = d.Start(runtime.GOMAXPROCS(-1)) // use several goroutines for faster decoding
	if err != nil {
		return nil, err
	}

	result := make(map[int64]Node)
	for {
		if v, err := d.Decode(); err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		} else {
			switch v := v.(type) {
			case *osmpbf.Node:
				result[v.ID] = Node{v.ID, Coord{v.Lon, v.Lat}, 0}
			}
		}
	}

	return result, nil
}

func readWays(filename string) (result []Way, err error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	d := osmpbf.NewDecoder(f)
	err = d.Start(runtime.GOMAXPROCS(-1)) // use several goroutines for faster decoding
	if err != nil {
		return nil, err
	}

	for {
		if v, err := d.Decode(); err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		} else {
			switch v := v.(type) {
			case *osmpbf.Way:
				p := NewEdgeProperties()
				for key, val := range v.Tags {
					p.update(key, val)
				}
				p.Normalize()
				if p.Accessible() {
					result = append(result, Way{v.ID, v.NodeIDs})
				}
			}
		}
	}

	return
}

func countNodesUses(nodes map[int64]Node, ways []Way) {
	for _, way := range ways {
		for i, node := range way.nodes {
			n := nodes[node]
			n.uses++
			// Count double extremities nodes
			if i == 0 || i == len(way.nodes)-1 {
				n.uses++
			}
			nodes[node] = n
		}
	}
}

func selectNodes(nodes map[int64]Node, ways []Way) map[int64]Node {
	result := make(map[int64]Node)
	for id, node := range nodes {
		if node.uses > 1 {
			result[id] = node
		}
	}

	return result
}

// Read all the nodes and ways of the osm.pbf file
func Read(filename string) (map[int64]Node, []Edge, error) {
	nodes, err := readNodes(filename)
	if err != nil {
		return nil, nil, err
	}
	ways, err := readWays(filename)
	if err != nil {
		return nil, nil, err
	}

	countNodesUses(nodes, ways)
	var edges []Edge
	for _, way := range ways {
		edges = append(edges, splitWay(way, nodes)...)
	}
	return selectNodes(nodes, ways), edges, nil
}
