package main

import (
	"encoding/csv"
	"fmt"
	"os"

	"github.com/tristramg/osm4routing2/reader"
)

func main() {
	nodes, ways, err := reader.Read("idf.osm.pbf")
	if err != nil {
		fmt.Printf("Error: %v", err)
	} else {
		fmt.Printf("Got %d nodes and %d ways\n", len(nodes), len(ways))
	}

	w := csv.NewWriter(os.Stdout)
	w.Write([]string{"wkt", "length"})
	for _, edge := range ways {
		w.Write(
			[]string{
				edge.AsWKT(),
				fmt.Sprintf("%.2f", edge.Length()),
			})
	}
}
